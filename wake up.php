<?php
class person
{
    public $name;
    public $address;
    public $phone;

    public function __sleep()
    {
        return array("name","phone");
    }

    public function __wakeup()
    {
        $this->dosomething();
    }
    public function dosomething(){

        echo"I am doing something<br>";
    }


}
$obj=new person();
$myVar=serialize($obj);
var_dump($myVar);
echo"<br>";

$newObject=unserialize($myVar);
echo"<br>";
var_dump($newObject);