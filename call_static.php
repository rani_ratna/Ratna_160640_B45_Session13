<?php
class person
{

    public $name;
    public $address;
    public $phone;
    public static $myStaticProperty;

    public static function dofrequently(){
        echo "I am doing it frequently<br>";
    }

    public static function __callStatic($name, $arguments)
    {
        echo "I am in".__METHOD__."<br>";
        echo"Wrong Static method=$name";
        echo"<pre>";
        print_r($arguments);
        echo"</pre>";
    }
}

person::$myStaticProperty="Hello static<br>";
echo person::$myStaticProperty;
person::dofrequently();
person::dofrequently1(123,"wrong static method parameter");